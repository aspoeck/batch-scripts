@echo off
set src_folder=c:\temp\LabelM\source
set dst_folder=c:\temp\LabelM\destination
set file_list=c:\temp\LabelM\txt\text.txt

if not exist "%dst_folder%" mkdir "%dst_folder%"

for /f "delims=" %%f in (%file_list%) do (
    xcopy "%src_folder%\%%f" "%dst_folder%\"
)


Pause